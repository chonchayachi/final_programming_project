#!/usr/bin/env python

import gzip
from Bio import SeqIO
import pandas as pd
import numpy as np
import argparse
        

def seq_record_to_dict(record):
    sequence = str(record.seq)
    phred_quality = record.letter_annotations["phred_quality"]
    description_parts = record.description.split(" ")
    description_dict = {}

    for part in description_parts:
        if "=" in part:
            key, value = part.split("=")
            description_dict[key] = value

    return {
        "id": record.id,
        "sequence_length": len(sequence),
        "mean_quality": np.mean(phred_quality),
        "start_time": description_dict.get("start_time", ""),
        "barcode": description_dict.get("barcode", "")
    }

###creates a DataFrame from the dictionaries
def fastq_to_csv(input_fastq_file, output_csv_file):
    records = []
    with gzip.open(input_fastq_file, 'rt') as bc_file:
        for record in SeqIO.parse(bc_file, 'fastq'):
            records.append(seq_record_to_dict(record))
    df = pd.DataFrame(records)
    df.to_csv(output_csv_file, index=False)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert FASTQ.gz to CSV")
    parser.add_argument("input_fastq_file", help="Input FASTQ.gz file")
    parser.add_argument("output_csv_file", help="Output CSV file")

    args = parser.parse_args()
    fastq_to_csv(args.input_fastq_file, args.output_csv_file)
