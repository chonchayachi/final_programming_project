from zipfile import ZipFile
import re
import gzip
from Bio import SeqIO


###EXAMPLE01

path_input = '/Users/chonchayachi/Desktop/SIRE504_programming/Term_project/data_set/example.zip'

with ZipFile(path_input) as zipf:
        for f in zipf.namelist():
            fInfo = zipf.getinfo(f)
            # print(fInfo)
            if not fInfo.is_dir():
                if re.search(".fastq.gz", f):
                    zipf.extract(fInfo.filename)


###barcode1
path_bc1 = "example/fastq/barcode01/ont.reads.bc01.fastq.gz"
with gzip.open(path_bc1, 'rt') as bc_1_file:
    recbc1 = SeqIO.parse(bc_1_file, 'fastq')
    for idx, record in enumerate(recbc1, 1):
         
        # print("ID:", record.id)
        # print("Description",record.description)
        # print("Sequence:", record.seq)
        # print("Quality scores:", record.letter_annotations["phred_quality"])
        print(record.letter_annotations)
        if idx == 1:
             break
    

        

####barcode2 
path_bc2 = "example/fastq/barcode02/ont.example.bc02.fastq.gz"
with gzip.open(path_bc2, 'rt') as bc_2_file:
    recbc2 = SeqIO.parse(bc_2_file, 'fastq')
    for idx, record in enumerate(recbc2, 1):
#         print(record.id, len(record))
#         print(record.letter_annotations)
#         print(type(record.id))
        print("ID:", record.id)
        print("Description",record.description)
        print("Sequence:", record.seq)
        print("Quality scores:", record.letter_annotations["phred_quality"])
        if idx == 1 :
             break

# ####barcode3
path_bc3 = "example/fastq/barcode03/ont.example.bc03.fastq.gz"
with gzip.open(path_bc3, 'rt') as bc_3_file:
    recbc3 = SeqIO.parse(bc_3_file, 'fastq')
    for idx, record in enumerate(recbc3, 1):
        # print(record.id, len(record))
        # print(record.letter_annotations)
        # print(type(record.id))
        print("ID:", record.id)
        print("Description",record.description)
        print("Sequence:", record.seq)
        print("Quality scores:", record.letter_annotations["phred_quality"])
        if idx == 1:
             break
        




###EXAMPLE02
path_input_example02 = '/Users/chonchayachi/Desktop/SIRE504_programming/Term_project/data_set/example2.zip'

with ZipFile(path_input_example02) as zipf:
        for f in zipf.namelist():
            fInfo = zipf.getinfo(f)
            # print(fInfo)
            if not fInfo.is_dir():
                if re.search(".fastq.gz", f):
                    zipf.extract(fInfo.filename)



path_ex02 = "example2/fastq/ont.exp2.fastq.gz"
with gzip.open(path_ex02, 'rt') as ex_02_file:
    ex_02_read = SeqIO.parse(ex_02_file, 'fastq')
    for idx, record in enumerate(ex_02_read, 1):
        print("ID:", record.id)
        print("Description:",record.description)
        print(type(record.description))
        print("Sequence:", record.seq)
        print("Quality scores:", record.letter_annotations["phred_quality"])
        print(record.letter_annotations)
        if idx == 1:
             break