#!/usr/bin/env python

import argparse
import pandas as pd
import gzip
from Bio import SeqIO
import json


### Create a criteria
def my_filter():
    parser = argparse.ArgumentParser(description='Filter data program')
    subparsers = parser.add_subparsers(title='commands', description='Please choose the command below:', dest='command')
    subparsers.required = True

    seq_parser = subparsers.add_parser('cutoffseq', help='Cut-off sequence')
    seq_parser.add_argument('-i', '--input', required=True, type=str, help='Input fastq file')
    seq_parser.add_argument('-c', '--csv', required=True, type=str, help='Input CSV file')
    seq_parser.add_argument('-n', '--number', required=True, type=int, help='number of barcode')
    seq_parser.add_argument('-q', '--quality', nargs='+', type=float, default=[0], help='Cut-off Quality')
    seq_parser.add_argument('-l', '--length', nargs='+', type=float, default=[0], help='Minimum sequence length cutoff')
    seq_parser.add_argument('--maxlength', nargs='+', type=float, default=[float('inf')], help='Maximum sequence length cutoff')

    return parser

### Create function to cutoff
def filter_cut_off_mean_quality(summary_table, cut_off_mean_quality, length, maxlength):
    return summary_table.loc[(summary_table['mean_quality'] >= cut_off_mean_quality)
        & (summary_table['sequence_length'] >= length)
        & (summary_table['sequence_length'] <= maxlength)]

### How to get barcode from description
def get_bar_code_from_record(record):
    description_parts = record.description.split(" ")
    description_dict = {}

    for part in description_parts:
        if "=" in part:
            key, value = part.split("=")
            description_dict[key] = value
    return description_dict['barcode']

###  Create dataframe from fastq files
def clean_fastq_seq_to_id_seq_pair(path):
    df = pd.DataFrame()
    with gzip.open(path, 'rt') as input_file:
        recbc = SeqIO.parse(input_file, 'fastq')
        ids = []
        seqs = []
        lengths = []
        qualities = []
        barcodes = []
        for record in recbc:
            ids.append(record.id)
            seqs.append(record.seq)
            lengths.append(len(record.seq))
            qualities.append(record.letter_annotations['phred_quality'])
            barcodes.append(get_bar_code_from_record(record))

    df['id'] = ids
    df['seq'] = seqs
    df['mean_quality'] = [sum(q) / len(q) for q in qualities]
    df['sequence_length'] = lengths
    df['barcode'] = barcodes
    return df

### Separate barcode
def divide_by_barcode(number_of_group, df):
    group_list = []
    for i in range(number_of_group):
        group_list.append(df.loc[df['barcode'] == f'barcode0{i + 1}'].reset_index())
    return group_list

###filter result from fastq.gz and csv
def get_final_seq_output(filter_result, id_seq_pair_df):
    result = {}
    pass_id = filter_result['id'].tolist()  #change to python list []

    for i in range(len(id_seq_pair_df)):
        if id_seq_pair_df.loc[i, 'id'] in pass_id:
            result[id_seq_pair_df.loc[i, 'id']] = {
                'sequence': str(id_seq_pair_df.loc[i, 'seq']),
                'mean_quality': float(id_seq_pair_df.loc[i, 'mean_quality']),
                'sequence_length': int(id_seq_pair_df.loc[i, 'sequence_length']),
                'barcode': str(id_seq_pair_df.loc[i, 'barcode'])
            }
    return result

def main():
    parser = my_filter()
    args = parser.parse_args()

    summary_df = pd.read_csv(args.csv)
    id_seq_pair_df = clean_fastq_seq_to_id_seq_pair(args.input)

    summary_table_list = divide_by_barcode(args.number, summary_df)
    id_seq_pair_df_list = divide_by_barcode(args.number, id_seq_pair_df)

    for i in range(args.number):
        filter_result = filter_cut_off_mean_quality(summary_table_list[i], args.quality[i], args.length[i], args.maxlength[i])
        output = get_final_seq_output(filter_result, id_seq_pair_df_list[i])
        with open(f'filter_sequence_{i + 1}.json', 'w') as f:
            json.dump(output, f)

if __name__ == '__main__':
    main()


