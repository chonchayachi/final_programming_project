#!/usr/bin/env python


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import argparse


class Genplot:
    def __init__(self):
        pass
    def cal_seq_length(self, df):
        seqlen_stats_list = []

        groupbc_df = df.groupby('barcode')

        for barcode, barcode_data in groupbc_df:
            seqlen_stats = barcode_data['sequence_length'].describe()
            seqlen_stats_rounded = seqlen_stats.round(2).to_frame().T
            seqlen_stats_rounded.drop(['mean', 'std', 'min', 'max'], axis='columns', inplace=True)

            sorted_lengths = sorted(barcode_data['sequence_length'])
            total_length = sum(sorted_lengths)
            cumulative_sum = 0
            n50 = None

            for length in sorted_lengths:
                cumulative_sum += length

                if cumulative_sum >= total_length / 2:
                    n50 = length 
                    break

            seqlen_stats_rounded['N50'] = n50

            print(f"Descriptive Statistics for Sequence Length - {barcode}:")
            print(seqlen_stats_rounded)

            seqlen_stats_list.append(seqlen_stats_rounded)

        return pd.concat(seqlen_stats_list, keys=groupbc_df.groups.keys())

    def cal_mean_qual(self, df):
        mean_quality_stats_list = []

        grouped_df = df.groupby('barcode')

        for barcode, barcode_data in grouped_df:
            mean_quality_stats = barcode_data['mean_quality'].describe()
            mean_quality_stats_rounded = mean_quality_stats.round(2).to_frame().T

            print(f"Descriptive Statistics for Mean Quality - {barcode}:")
            print(mean_quality_stats_rounded)

            mean_quality_stats_list.append(mean_quality_stats_rounded)

        return pd.concat(mean_quality_stats_list, keys=grouped_df.groups.keys())
    
    def generate_plot(self, df, output_length_plot, output_qual_plot):
        grouped_df = df.groupby('barcode')

        for barcode, barcode_data in grouped_df:
            
            # Plot sequence lengths for barcode 
            length_fig, ax_length = plt.subplots(figsize=(10, 6))
            sns.kdeplot(barcode_data['sequence_length'], fill=True, color='skyblue', ax=ax_length, log_scale=True)
        
            percentiles_bc = [10, 25, 50, 75, 90]
            percentile_values_bc = np.percentile(barcode_data['sequence_length'], percentiles_bc)
            
            ax_length.axvline(percentile_values_bc[0], color='orange', linestyle='--', label=f'10th percentile ({percentile_values_bc[0]:.2f})')
            ax_length.axvline(percentile_values_bc[1], color='green', linestyle='--', label=f'25th percentile ({percentile_values_bc[1]:.2f})')
            ax_length.axvline(percentile_values_bc[2], color='red', linestyle='--', label=f'50th percentile (Median) ({percentile_values_bc[2]:.2f})')
            ax_length.axvline(percentile_values_bc[3], color='blue', linestyle='--', label=f'75th percentile ({percentile_values_bc[3]:.2f})')
            ax_length.axvline(percentile_values_bc[4], color='purple', linestyle='--', label=f'90th percentile ({percentile_values_bc[4]:.2f})')
            
            ax_length.set_xlabel('Sequence Length (log scale)')
            ax_length.set_ylabel('Density')
            ax_length.set_title(f'Distribution of Sequence Lengths - {barcode}')
            ax_length.legend()


            # Plot mean quality
            qual_fig, ax_qual = plt.subplots(figsize=(10, 6))
            sns.kdeplot(barcode_data['mean_quality'], fill=True, color='skyblue', ax=ax_qual)
            
            percentiles_bc_qual = [10, 25, 50, 75, 90]
            percentile_values_qual = np.percentile(barcode_data['mean_quality'], percentiles_bc_qual)
            
            ax_qual.axvline(percentile_values_qual[0], color='orange', linestyle='--', label=f'10th percentile ({percentile_values_qual[0]:.2f})')
            ax_qual.axvline(percentile_values_qual[1], color='green', linestyle='--', label=f'25th percentile ({percentile_values_qual[1]:.2f})')
            ax_qual.axvline(percentile_values_qual[2], color='red', linestyle='--', label=f'50th percentile (Median) ({percentile_values_qual[2]:.2f})')
            ax_qual.axvline(percentile_values_qual[3], color='blue', linestyle='--', label=f'75th percentile ({percentile_values_qual[3]:.2f})')
            ax_qual.axvline(percentile_values_qual[4], color='purple', linestyle='--', label=f'90th percentile ({percentile_values_qual[4]:.2f})')
            
            ax_qual.set_xlabel('Quality scores')
            ax_qual.set_ylabel('Density')
            ax_qual.set_title(f'Distribution of Quality scores - {barcode}')
            ax_qual.legend()

            # Save plots
            length_fig.savefig(output_length_plot.replace('.png', f'_{barcode}.png'))
            
            qual_fig.savefig(output_qual_plot.replace('.png', f'_{barcode}.png'))
  
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate sequence length and mean quality plots for different barcodes.')
    parser.add_argument('csv_file', type=str, help='Input CSV file')
    parser.add_argument('output_length_plot', type=str, help='Output file for sequence length plot')
    parser.add_argument('output_qual_plot', type=str, help='Output file for mean quality plot')
    args = parser.parse_args()

    df = pd.read_csv(args.csv_file)
    gen_plot = Genplot()
    gen_plot.cal_seq_length(df)
    gen_plot.cal_mean_qual(df)
    gen_plot.generate_plot(df, args.output_length_plot, args.output_qual_plot)






