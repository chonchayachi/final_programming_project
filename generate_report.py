#!/usr/bin/env python

import pandas as pd
import sys
from generate_plot import Genplot


def generate_report(input_file, output_length_plot, output_qual_plot, output_file):
    df = pd.read_csv(input_file)

    genplot = Genplot()

    # Group by the 'barcode' column
    groupbc_df = df.groupby('barcode')

    # Dictionaries to store stats for each barcode
    callen_stats = {}
    calmeanqual_stats = {}

    # Calculate statistics for each barcode
    for barcode, barcode_data in groupbc_df:
        callen_stats[barcode] = genplot.cal_seq_length(barcode_data)
        calmeanqual_stats[barcode] = genplot.cal_mean_qual(barcode_data)

    # Extract the first 5 lines of the CSV data for the example table
    example_table = df.head(5).to_html(index=False)

    # Generate length plot and quality plot for each barcode
    for barcode, barcode_data in groupbc_df:
        genplot.generate_plot(barcode_data, 
                              f"{output_length_plot.replace('.png', f'_{barcode}.png')}", 
                              f"{output_qual_plot.replace('.png', f'_{barcode}.png')}")

    # Create HTML report 
    html_content = f"""
    <!DOCTYPE html>
    <html>
    <head>
        <title>SEQUENCING Report</title>
        <style>
            body {{
                font-family: Arial, sans-serif;
                text-align: center;
                margin: auto;
            }}
            h1, h2 {{
                color: #333;
            }}
            img {{
                max-width: 100%;
                height: auto;
                margin-top: 20px;
                display: block;
                margin-left: auto;
                margin-right: auto;
            }}
            table {{
                width: 80%;
                margin: 20px auto;
                border-collapse: collapse;
            }}
            th, td {{
                border: 1px solid #ddd;
                padding: 8px;
                text-align: left;
            }}
            th {{
                background-color: #f2f2f2;
            }}
        </style>
    </head>
    <body>
        <h1>SEQUENCING Report</h1>
        <h2>Example of the data</h2>
        {example_table}
        <br></br>
        <h2>Summary Statistics</h2>
        <br></br>
    """

    # Generate HTML content for each barcode
    for barcode, barcode_data in groupbc_df:
        html_content += f"""
        <h2>Basecalled reads Length - {barcode}</h2>
        {callen_stats[barcode].to_html(index=False)}
        <h3>Sequence Length Distribution and Percentiles</h3>
        <img src="{output_length_plot.replace('.png', f'_{barcode}.png')}" alt="Sequence Length Distribution - Barcode {barcode}">
        <br></br>
        <br></br>
        <h2>Basecalled reads Quality scores - {barcode}</h2>
        {calmeanqual_stats[barcode].to_html(index=False)}
        <h3>Mean Quality Score Distribution and Percentiles</h3>
        <img src="{output_qual_plot.replace('.png', f'_{barcode}.png')}" alt="Mean Quality Score Distribution - Barcode {barcode}">
        <br></br>
        <br></br>
        """

    # Save the HTML report to the output file
    with open(output_file, 'w') as f:
        f.write(html_content)

    print(f"Report generated successfully. Open {output_file} in a web browser.")

if __name__ == "__main__":
    input_file = sys.argv[1]
    output_length_file = sys.argv[2]
    output_qual_file = sys.argv[3]
    output_file = sys.argv[4]

    generate_report(input_file, output_length_file, output_qual_file, output_file)


